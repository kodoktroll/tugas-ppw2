from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
from .models import Tanggapan
from django.core import serializers
import json

# Create your views here.

response = {}

# blum selese baru bisa login ke linkedIn doang


def index(request):
    html = 'fitur1/fitur1.html'
    tanggapan = Tanggapan.objects.all()
    response['komentar'] = tanggapan
    return render(request, html, response)

@csrf_exempt
def add_comment(request):
    if request.method == 'POST':
        nama = request.POST['nama']
        komentar = request.POST['komentar']

        comment = Tanggapan(nama=nama, komentar=komentar)
        comment.save()
        data = model_to_dict(comment)
        return HttpResponse(data)
    else:
        return HttpResponseRedirect(reverse('fitur1:index'))

def model_to_dict(obj):
    data = serializers.serialize('json', [obj, ])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data
