from django.test import TestCase
from django.test import TestCase, Client
from django.urls import resolve
from .views import index, add_comment

# Create your tests here.
class Fitur1UnitTestCase(TestCase):
    def test_fitur_1_url_is_exist(self):
        response = Client().get('/landing/')
        self.assertEqual(response.status_code, 200)

    def test_fitur_1_using_index_func(self):
        found = resolve('/landing/')
        self.assertEqual(found.func, index)

    def test_add_comment(self):
        response_post = Client().post(
            '/landing/add-comment/',
            {'nama': "test", 'komentar': "1234"}
        )
        self.assertEqual(response_post.status_code, 200)