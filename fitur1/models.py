from django.db import models

class Lowongan(models.Model):
    perusahaan = models.CharField(max_length=250)
    lowongan = models.TextField()

class Tanggapan(models.Model):
    nama = models.CharField(max_length=250)
    komentar = models.TextField()
