from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from .models import Company
from django.utils import timezone
import json

# Create your views here.

response = {}


def index(request):
    html = 'fitur2/profile.html'
    name = request.GET
    print(name)
    return render(request, html, response)


@csrf_exempt
def add_company(request):
    if request.method == 'POST':
        name = request.POST['name']
        aidi = request.POST['id']
        exist = Company.objects.filter(aidi__iexact=aidi).exists()
        # print(Company.objects.all)
        if(not exist):
            company = Company()
            company.name = name
            company.aidi = aidi
            company.added_at = timezone.now()
            company.save()
    data = model_to_dict(company)
    return HttpResponse(data)


def model_to_dict(obj):
    data = serializers.serialize('json', [obj, ])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data
