from django.db import models

# Create your models here.


class Company(models.Model):
    name = models.CharField(max_length=400)
    aidi = models.CharField(max_length=250)
    added_at = models.DateField(auto_now_add=True, null=True, blank=True)
    #created_date = models.DateTimeField(auto_now_add=True)
