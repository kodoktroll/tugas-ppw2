from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, model_to_dict
from .models import Company


class fitur2UnitTest(TestCase):

    def test_lab_7_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_model_to_dict(self):
        new_activity = Company.objects.create(
            name='Ezza', aidi='1000000')
        data = model_to_dict(new_activity)
        self.assertEqual
        ('{"name": "0606101452", "id": "100000",}', data)
        # Create your tests here.

    def test_add_friend(self):
        response_post = Client().post(
            '/profile/add-company/',
            {'name': "ezza", 'id': "10000"}
        )
        self.assertEqual(response_post.status_code, 200)
