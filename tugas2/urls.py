"""tugas2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import fitur1.urls as fitur1
import fitur2.urls as fitur2
#import fitur3.urls as fitur3
from django.views.generic import RedirectView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(
        url='/landing/', permanent=True)),
    url(r'^fitur1/', RedirectView.as_view(
        url='/landing/', permanent=True)),
    url(r'^landing/', include(fitur1, namespace='fitur1')),
    #url(r'', include(fitur3, namespace='fitur3')),
    url(r'^profile/', include(fitur2, namespace="fitur2"))

]
