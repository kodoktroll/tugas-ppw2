function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
    IN.Event.on(IN, "auth", checkAdmin);

}
function checkAdmin(companyID){
  var cpnyID = companyID;
  IN.API.Raw("/companies/" + cpnyID + "/relation-to-viewer/is-company-share-enabled?format=json").method("GET").result(displayAdmin).error(onError);


}
function displayAdmin(data){
  console.log("admin" + data)

}


// Use the API call wrapper to request the member's profile data
function getCompanyProfileData(companyID) {
    var cpnyID = companyID;
    IN.API.Raw("/companies/" + cpnyID + ":(id,name,website-url,description,company-type,founded-year,specialties,logo-url)?format=json").method("GET").result(displayCompanyProfileData).error(onError);

}

//Handle the successful return from the API call

function displayCompanyProfileData(data){
    console.log(data);
    document.getElementById("judul").append("Selamat Datang di, " + data.name);
    document.getElementById("companyName").innerHTML = "Nama Perusahaan :" + data.name;
    document.getElementById("description").innerHTML = "Deskripsi Perusahaan: " + data.description;
    document.getElementById("companyType").innerHTML = "Tipe Perusahaan: "+data.companyType.name;
    document.getElementById("website").innerHTML = "Website Perusahaan: "+data.websiteUrl;
    document.getElementById("specialties").innerHTML = "Spesialiti Perusahaan: "+data.specialties.values[0];
    document.getElementById("picture").innerHTML =  '<img src="' + data.logoUrl+ '" alt="theFace"">'
    saveCompanyToDatabase(data.name, data.id)

}

// Use the API call wrapper to request the member's profile data
function getProfileData() {
    IN.API.Profile("me").fields("id", "first-name", "last-name", "headline", "location", "picture-url", "public-profile-url", "email-address", "positions").result(displayProfileData).error(onError);
}

// Handle the successful return from the API call
function displayProfileData(data){
    var user = data.values[0];
    console.log(data)
    var id = user.positions.values[0].company.id;
    console.log(id)
    getCompanyProfileData(id)

}

//saving to database
function saveCompanyToDatabase(nama, id){
    $.ajax({
        method: "POST",
        url: 'http://localhost:8000/profile/add-company/',
        data: {nama:nama, id:id},
        success: function(){
            console.log(nama + " " + id + " berhasil ditambahkan")
        },
        error: function(error){
            console.log("gagal")
        }
    })
}

// Handle an error response from the API call
function onError(error) {
    console.log(error);
}

// Destroy the session of linkedin
function logout(){
    IN.User.logout();
    window.location.replace("http://localhost:8000/");
}

// Remove profile data from page
function removeProfileData(){
    document.getElementById('profileData').remove();
}

$.LoadingOverlay("show", {
color           : "rgba(17, 15, 15)" ,   // String
custom          : ""                           , // String/DOM Element/jQuery Object
fade            : true                        ,  // Boolean/Integer/String/Array
fontawesome     : ""                           , // String
imagePosition   : "center center"               ,// String
maxSize         : "100px"                       ,// Integer/String
minSize         : "20px"                        ,// Integer/String
resizeInterval  : 50                           , // Integer
size            : "50%"                        , // Integer/String
zIndex          : 9999                          ,// Integer

});



$(document).ajaxStop(function(){
    $.LoadingOverlay("hide");
});
